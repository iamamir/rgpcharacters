package com.company.items;

import com.company.models.BodyPart;

/**
 * abstract super class that is responsible items and its different types as well as the different operations
 */
public abstract class Item {

    protected String name;
    protected int level;
    protected ItemsType itemsType;
    protected BodyPart bodyPart;

    public Item(String name, int level, ItemsType itemsType, BodyPart bodyPart) {
        this.name = name;
        this.level = level;
        this.itemsType = itemsType;
        this.bodyPart = bodyPart;
    }

    /**
     * to set the level of an item, passing level in the parameter
     * @param level
     */
    public abstract void setLevel(int level);

    /**
     * to update the stats of an item
     */
    public abstract void scaleUp();

    /**
     * to print the details of a chosen item
     */
    public abstract void printDetails();

    /**
     * to get the name of an item
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * to get the level of an item
     * @return
     */
    public int getLevel() {
        return level;
    }

    /**
     * to get the type of an item, if it's leather, plate, cloth, melee etc..
     * @return
     */
    public ItemsType getItemsType() {
        return itemsType;
    }



}
