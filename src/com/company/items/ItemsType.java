package com.company.items;

/**
 * it identifies the different types of items' name
 */
public enum ItemsType {
    Melee,
    Ranged,
    Magic,
    Weapon,
    Cloth,
    Leather,
    Plate
}
