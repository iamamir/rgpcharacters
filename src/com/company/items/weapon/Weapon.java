package com.company.items.weapon;

import com.company.items.Item;
import com.company.items.ItemsType;
import com.company.models.BodyPart;
import com.company.models.PrintFormatter;
import com.company.models.Stats;

/**
 * Weapon a subclass of items, and abstract super class for the weapons.
 * responsible for the weapon type items, and its operations
 */
public abstract class Weapon extends Item {

    // the weapon damage
    protected int damage;

    /**
     * weapons are created level 1 by default and all they set on hands
     * @param name the name of the weapon
     * @param itemsType either, melee, ranged or magic
     */
    public Weapon(String name, ItemsType itemsType) {
        super(name, 1, itemsType, BodyPart.Hand);
    }

    /**
     * to update the damage of the weapon when equipped
     * @param stat hero's stats
     */
    public abstract void updateDamageWhenEquipped(Stats stat);

    /**
     * to update the damage of the weapon when unequipped
     * @param stat hero's stats
     */
    public abstract void updateDamageWhenUnEquipped(Stats stat);

    /**
     * to set the level, and once the level is sat, it updates the damage
     * @param level
     */
    @Override
    public void setLevel(int level) {
        this.level = level;
        if (level > 1)
            scaleUp();
    }

    /**
     * to update the damage according to the increased levels
     */
    @Override
    public abstract void scaleUp();

    /**
     * will return the weapon damage
     * @return
     */
    public int getDamage() {
        return damage;
    }

    /**
     * to print the details of specific weapon item
     */
    public void printDetails() {
        PrintFormatter pf = new PrintFormatter();

        String first = "+*********** Weapon Details ***********+";
        System.out.println(first);
        pf.formatText(first.length(), "| Item stats for: " + this.getName());
        pf.formatText(first.length(), "| Weapon type: " + itemsType);
        pf.formatText(first.length(), "| Weapon level: " + this.getLevel());
        pf.formatText(first.length(), "| Damage: " + this.getDamage());
        System.out.println("+**************************************+");

    }


}
