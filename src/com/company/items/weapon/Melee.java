package com.company.items.weapon;

import com.company.items.ItemsType;
import com.company.models.Stats;
/**
 * Melee, is a subclass of a weapon and a grandchild of items
 */
public class Melee extends Weapon {

    /**
     * the constructor sets the basic damage of the weapon as well as the name, and the type of the weapon
     * @param name
     */
    public Melee(String name) {
        super(name, ItemsType.Melee);

        damage = 15;

    }


    /**
     * to update the damage of the weapon when equipped
     * @param stat it takes the stats of the hero equipped it
     */
    @Override
    public void updateDamageWhenEquipped(Stats stat) {
        damage = damage + (int) (1.5 * stat.getStrength());

    }

    /**
     * to set the damage back to normal when unequipped
     * @param stat it takes the stats of the hero equipped it
     */
    @Override
    public void updateDamageWhenUnEquipped(Stats stat) {
        damage = damage - (int) (1.5 * stat.getStrength());

    }

    /**
     * to update the damage when the level is updated
     */
    @Override
    public void scaleUp() {
        damage = damage + (2 * level);
    }
}
