package com.company.items.weapon;

import com.company.items.ItemsType;
import com.company.models.Stats;

/**
 * Magic, is a subclass of a weapon and a grandchild of items
 */
public class Magic extends Weapon {

    /**
     * the constructor sets the basic damage of the weapon as well as the name, and the type of the weapon
     * @param name
     */
    public Magic(String name) {
        super(name, ItemsType.Magic);
        damage = 25;

    }

    /**
     * to update the damage of the weapon when equipped
     * @param stat it takes the stats of the hero equipped it
     */
    @Override
    public void updateDamageWhenEquipped(Stats stat) {
        damage = damage + (3*stat.getIntelligence());
    }

    /**
     * to set the damage back to normal when unequipped
     * @param stat it takes the stats of the hero equipped it
     */
    @Override
    public void updateDamageWhenUnEquipped(Stats stat) {
        damage = damage - (3*stat.getIntelligence());
    }

    /**
     * to update the damage when the level is updated
     */
    @Override
    public void scaleUp() {
        damage = damage + (2 * level);
    }
}
