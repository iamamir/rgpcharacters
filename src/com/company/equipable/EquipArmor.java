package com.company.equipable;

import com.company.items.ItemsType;
import com.company.models.Inventory;
import com.company.items.Item;

/**
 * this class is responsible for equipping and unequipping the armors types
 */
public class EquipArmor implements Equipable {
    /**
     *
     * @param item it is the item to be equipped
     * @param inventory refers to the bag/inventory where the user can equips items to.
     * @return it returns true if the item is successfully equipped, meaning that the inventory does not have an item of the same type and the size is smaller of equal to 4
     *          it calls the inventory add method to add an item.
     */
    @Override
    public boolean equip(Item item, Inventory inventory) {
        return inventory.addItem(item.getItemsType(),item);

    }

    /**
     *
     * @param itemsType the type of to be removed item,
     * @param inventory the inventory where items are stored
     *                  it calls the inventory remove method, to remove the item
     */
    @Override
    public void unequip(ItemsType itemsType, Inventory inventory) {
        inventory.removeItem(itemsType);


    }
}
