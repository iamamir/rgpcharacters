package com.company.equipable;

import com.company.items.ItemsType;
import com.company.models.Inventory;
import com.company.items.Item;

/**
 * interface to implement the operation of equipping and unequipping different items, namely weapons and armors.
 */
public interface Equipable {

    boolean equip(Item item, Inventory inventory);
    void unequip(ItemsType itemsType, Inventory inventory);
}
