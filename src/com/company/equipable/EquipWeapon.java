package com.company.equipable;

import com.company.items.ItemsType;
import com.company.models.Inventory;
import com.company.items.Item;
/**
 * this class is responsible for equipping and unequipping the weapon types
 */
public class EquipWeapon implements Equipable {

    /**
     *
     * @param item it is the item to be equipped
     * @param inventory refers to the bag/inventory where the user can equips items to.
     * @return it returns true if the item is successfully equipped, meaning that the inventory does not have an item of the same type and the size is smaller of equal to 4
     *          it calls the inventory add method to add an item.
     *
     *          for weapons it's hardcoded type weapon, to limit the user to only equip one type of a weapon.
     */
    @Override
    public boolean equip(Item item, Inventory inventory) {
        return inventory.addItem(ItemsType.Weapon,item);

    }

    /**
     *
     * @param itemsType the type here is not used, but it rather serves the EquipArmor class
     * @param inventory the inventory where items are stored
     *                  it calls the inventory remove method, to remove the item of
     */
    @Override
    public void unequip(ItemsType itemsType, Inventory inventory) {
        inventory.removeItem(ItemsType.Weapon);

    }

}
