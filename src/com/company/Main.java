package com.company;

import com.company.heros.Hero;
import com.company.heros.HeroFactory;
import com.company.heros.HeroTypes;
import com.company.items.Item;
import com.company.items.ItemsFactory;
import com.company.items.ItemsType;
import com.company.models.BodyPart;

public class Main {

    public static void main(String[] args) {

        // creating three characters, one of each.
        Hero mage = new HeroFactory().createHero(HeroTypes.Mage, "Harry Potter");
        Hero warrior = new HeroFactory().createHero(HeroTypes.Warrior, "Black Panther");
        Hero ranger = new HeroFactory().createHero(HeroTypes.Ranger, "Captain America");

        // creating three weapons, one of each.
        Item ranged = new ItemsFactory().createItem(ItemsType.Ranged, "Great Ranged", BodyPart.Hand);
        Item melee = new ItemsFactory().createItem(ItemsType.Melee, "Great Melee", BodyPart.Hand);
        Item magic = new ItemsFactory().createItem(ItemsType.Magic, "Great Magic", BodyPart.Hand);


        // creating three armors, one of each.
        Item cloth = new ItemsFactory().createItem(ItemsType.Cloth, "Great cloth", BodyPart.Torso);
        Item plate = new ItemsFactory().createItem(ItemsType.Plate, "Great Plate", BodyPart.Head);
        Item leather = new ItemsFactory().createItem(ItemsType.Leather, "Great Leather", BodyPart.Legs);

        //an extra armor
        Item cloth2 = new ItemsFactory().createItem(ItemsType.Cloth, "Great cloth2", BodyPart.Head);


        System.out.println("printing the details of all three characters before and after gaining xp.");
        mage.printDetails();
        mage.gainXp(500);
        mage.printDetails();
        System.out.println("----------------------------------------");
        warrior.printDetails();
        warrior.gainXp(222);
        warrior.printDetails();
        System.out.println("----------------------------------------");
        ranger.printDetails();
        ranger.gainXp(340);
        ranger.printDetails();
        System.out.println("----------------------------------------");


        System.out.println("printing the details of all three weapons before and after setting the level.");
        magic.printDetails();
        magic.setLevel(2);
        magic.printDetails();
        System.out.println("----------------------------------------");
        melee.printDetails();
        melee.setLevel(3);
        melee.printDetails();
        System.out.println("----------------------------------------");
        ranged.printDetails();
        ranged.setLevel(4);
        ranged.printDetails();
        System.out.println("----------------------------------------");

        System.out.println("printing the details of all three armors before and after setting the level.");
        cloth.printDetails();
        cloth.setLevel(2);
        cloth.printDetails();
        System.out.println("----------------------------------------");
        plate.printDetails();
        plate.setLevel(3);
        plate.printDetails();
        System.out.println("----------------------------------------");
        leather.printDetails();
        leather.setLevel(4);
        leather.printDetails();
        System.out.println("----------------------------------------");

        System.out.println("Equipping items (weapon/armors) on one of the characters and showing the change in stats");
        System.out.println("Nothing equipped yet");
        mage.printDetails();

        System.out.println("Details with equipped cloth lvl2 on torso");
        mage.equip(cloth);
        mage.printDetails();

        System.out.println("Details with equipped cloth lvl2 on torso, plate lvl 3 on head");
        mage.equip(plate);
        mage.printDetails();

        System.out.println("Details with equipped cloth lvl2 on torso, plate lvl 3 on head, leather lvl 4 on legs");
        mage.equip(leather);
        mage.printDetails();

        System.out.println("Details with equipped cloth lvl2 on torso, plate lvl 3 on head, leather lvl 4 on legs and magic lvl 2 on hand");
        mage.equip(magic);
        mage.printDetails();

        System.out.println("attacking details");
        mage.attack();

        System.out.println("Equipping a different cloth lvl 1 on torso while already wearing lvl 2 one on torso (replacing)");
        mage.equip(cloth2);
        mage.printDetails();

    }
}
