package com.company.heros;

/**
 * this class is used to create heroes
 */
public class HeroFactory {

    public Hero createHero(HeroTypes type, String name) {
        return switch (type) {
            case Mage -> new Mage(name);
            case Warrior -> new Warrior(name);
            case Ranger -> new Ranger(name);
            default -> null;
        };

    }



}
