package com.company.heros;

/**
 * enum to be used identify the different hero types on creation
 */
public enum HeroTypes {
    Warrior,
    Ranger,
    Mage
}
