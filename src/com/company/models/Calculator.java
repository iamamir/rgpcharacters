
package com.company.models;

/**
 * this class to perform the calculation of adding and removing the effectivestats when
 * equipping and unequipping an armor item
 */
public class Calculator {

    public Stats getEffectiveStats(Stats itemStats, Stats heroStats) {
        return new Stats(
                (heroStats.getHealth() + itemStats.getHealth()),
                (heroStats.getStrength() + itemStats.getStrength()),
                (heroStats.getDexterity() + itemStats.getDexterity()),
                (heroStats.getIntelligence() + itemStats.getIntelligence())
        );
    }

    public Stats removeEffectiveStats(Stats itemStats, Stats heroStats) {
        return new Stats(
                (heroStats.getHealth() - itemStats.getHealth()),
                (heroStats.getStrength() - itemStats.getStrength()),
                (heroStats.getDexterity() - itemStats.getDexterity()),
                (heroStats.getIntelligence() - itemStats.getIntelligence())
        );
    }

}
