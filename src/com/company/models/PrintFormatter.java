package com.company.models;

/**
 * model class to print a cool design of printed details of either, heroes or items.
 */
public class PrintFormatter {

    public void formatText(int len, String input) {
        String padded = String.format("%-" + (len - 1) + "s", input);
        System.out.println(padded + "|");
    }
}
