package com.company.models;

/**
 * it identifies the different bodyparts of the hero, one which items could be equipped
 */
public enum BodyPart {
    Hand,
    Head,
    Torso,
    Legs,
}
