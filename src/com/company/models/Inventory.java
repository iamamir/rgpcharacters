package com.company.models;

import com.company.items.Item;
import com.company.items.ItemsType;

import java.util.HashMap;

/**
 * this is the inventory model class, responsible for storing equipped items
 */
public class Inventory {

    // a hashmap it takes the itemtype as a key, and the item as a value
    // in this way, u can only equip one type of an item , it ensures that the hero
    // cannot equip multiple items of the same type
    HashMap<ItemsType, Item> inventory;

    public Inventory() {
        inventory = new HashMap<>();
    }


    /**
     * to add an item to the map
     * @param itemsType it takes the itemtype which is the key
     * @param item the item which is the value
     *             then it checks if this item is not already in the map and the map is less than4
     *             it then added the item to the map and return true
     * @return      false is returned if the condition is not satisfied
     */
    public boolean addItem(ItemsType itemsType, Item item) {
        if (!inventory.containsKey(itemsType)&& inventory.size() <= 4) {
            inventory.put(itemsType, item);
            return true;
        } else
            return false;
    }

    /**
     * to remove an item from the map
     * @param itemsType
     */
    public void removeItem(ItemsType itemsType) {
        inventory.remove(itemsType);
    }

    /**
     * to get a specific item
     * @param itemsType it takes the type of the needed item
     * @return and return the value of the key which is the item
     */
    public Item getSpecificItem(ItemsType itemsType){
        return inventory.get(itemsType);
    }

    /**
     * to get the whole map
     * @return
     */
    public HashMap<ItemsType, Item> getInventory() {
        return inventory;
    }
}
