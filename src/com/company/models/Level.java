package com.company.models;

/**
 * the level model class, responsible for calculating the gained xp and setting the target xp of each level
 */
public class Level {

    // by default heroes starts at level 1 with xp 0 and 100 as the target in order to level up
    public int level = 1;
    public int xp = 0;
    public int targetXp = 100;


    /**
     * this method it takes in xp and it updates the attributes, and then sets the target xp for the
     * next level, in case there's enough xp to level up.
     * and then it returns the number of levels corresponding to the gained xp
     * @param gainxp
     * @return
     */
    public int gainXp(int gainxp) {
        int counter = 0;

        while (gainxp != 0) {
            int remainingXp = targetXp - xp;

            if (remainingXp == gainxp) {
                updateLvl();
                counter++;
            }

            if (remainingXp > gainxp) {
                xp = gainxp;
                gainxp = 0;
            }

            if (remainingXp < gainxp) {
                xp = remainingXp;
                updateLvl();
                gainxp = gainxp - remainingXp;
                counter++;
            }
        }
        return counter;
    }

    /**
     * to update the level, reset xp and set a new targetxp
     */
    private void updateLvl() {
        level++;
        xp = 0;
        targetXp = (int) (targetXp * 1.1);


    }

}
