# RGP Characters
## RGP Characters is console application game
---
### The User can create between three different characters, namely, Warrior, Ranger and Mage. Each character has different starting stats, character gain stats through leveling up, as well as equipping items. Furthermore, the character gains dealt damage when equipping a weapon item.
### Each character can equip up to four items, there are different types of items:
1. Armors
* 1. Plate Armor
* 2. Cloth Armor
* 3. Leather Armor
2. Weapons
* 1. Ranged
* 2. Melee
* 3. Magic

### For this application, the character can only equip three different types of armors and one type of weapon, when trying to equip the same type of armor or a different type of weapon, it will only replace the already equipped with the desired ones.
### Just like the character, the stats of the items change when the level is updated.
### A hero can only equip same level of lower level items.
### Below are samples of how the game looks during run time:
![Screenshot](https://gitlab.com/iamamir/rgpcharacters/-/raw/master/src/resources/hero.png)
![Screenshot](https://gitlab.com/iamamir/rgpcharacters/-/raw/master/src/resources/weapon.PNG)
![Screenshot](https://gitlab.com/iamamir/rgpcharacters/-/raw/master/src/resources/Armor.PNG)


